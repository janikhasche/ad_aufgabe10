package hashtable;

import java.lang.reflect.Array;

public class DoubleHashTable<T> implements HashTable<T> {
	
	private Data[] elements;
	private int size;

	public DoubleHashTable() {
		elements = createArray(15);
		size = 0;
	}
	
	
	@Override
	public void insert(long index, T data) {
		
		//check loadFactor
		size++;
        double loadFactor = size / elements.length;
        if(loadFactor > 0.8){
        	elements = getResizedArray();
        }
        
        insertToHashArray(index, data, elements);	
	}
	
	@SuppressWarnings("unchecked")
	private Data[] createArray(int length){
		Data data = new Data(0, null);
		return (Data[]) Array.newInstance(data.getClass(), length);
	}


	private void insertToHashArray(long index, T content, Data[] hashArray) {
		Data insertData = new Data(index, content);
		
		boolean hashOk = false;
        long i = 0;
        do{
        	int hashIndex =  hashFnc(i, index, hashArray.length);
        	if(hashArray[hashIndex] == null){
        		hashArray[hashIndex] = insertData;
        		hashOk = true;
        	}else{
        		i++;
        		if(i > hashArray.length){
        			Data[] resizedArray = getResizedArray();
        			hashArray = resizedArray;
        			elements = resizedArray;
        			i = 1;
        			
        		}
        	}   	
        } while(!hashOk);
	}


	@Override
	public T get(long key) {
		for(int i = 1; i <= elements.length; i++){
			int hashIndex = hashFnc(i, key, elements.length);
			Data data = elements[hashIndex];
			if((data != null) && (data.key == key)){
				return data.content;
			}	
		}
		return null;
	}
	
	
    private int hashFnc(long i , long key, int arrSize){
        long m = getPrime(arrSize); //arrSize
        long hk = key % m; //h(k)
        long hks = 1 + (key % (m - 2)); //h(k)'
        int hik = (int) ((hk + hks * (i*i)) % m);
 
        return hik;
    }
    
    
    private Data[] getResizedArray(){
    	int resizeLength = getResizeLength(elements.length);
		Data[] newElements = createArray(resizeLength);
    	
    	// rehash elements to resized Array
    	for(Data elem: elements){
    		if(elem != null){
    			this.insertToHashArray(elem.key, elem.content, newElements);
    		}
    	}
    	
    	return newElements;
    }
    
    
    private int  getPrime(int arrSize_2) {
        int startVal = arrSize_2-1; //Warum -1? hastArr[x] = 59 und ArrSize auch 59
        for (int i = 2; i < arrSize_2-1; i++) {
            startVal++;
            if(startVal%i != 0){
               return startVal;
            }
        }
        return -1;
    }
    
    
    private int getResizeLength(int hashArrSize){
        int mNew = 2; // 0.9 / 0.5 = 1,8 ~ 2
        int newArrayLength = getPrime(hashArrSize * mNew);
 
        return newArrayLength;
    }
 
    
	private class Data{
		T content;
		long key;
		
		Data(long key, T content){
			this.key = key;
			this.content = content;
		}
	}
}
