package hashtable;

import java.util.HashMap;
import java.util.Map;

public class HashTableStub<T> implements HashTable<T> {
	
	Map<Long, T> stubMap;

	public HashTableStub() {
		stubMap = new HashMap<Long, T>();
	}

	@Override
	public void insert(long index, T data) {
		stubMap.put(index, data);
		
	}

	@Override
	public T get(long index) {
		return stubMap.get(index);
	}
}
