package hashtable;

public interface HashTable<T> {
	
	public void insert(long index, T Data);
	
	public T get(long index);
}
