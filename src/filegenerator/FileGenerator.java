package filegenerator;

import java.io.FileWriter;
import java.io.IOException;

public class FileGenerator {
	
    public static void main(String[] args) {
    	generateFile(5000, "LogFile.txt");
    }
    
    public static String generateIpAdress(){   	
    	//generate Numbers
    	int[] ipPart = new int[4];
    	for(int i = 0; i < ipPart.length; i++){
    		ipPart[i] = (int) (Math.random() * 255);
    	}
    	// build string from Numbers
    	String ipAdress = String.format("%d.%d.%d.%d", ipPart[0], ipPart[1], ipPart[2], ipPart[3]); 
    	return ipAdress;
    }  
    
    public static void generateFile(long fileLength, String fileName){   	
        try {   
        	FileWriter writer = new FileWriter(fileName, true);
	    	for(long i = 0; i < fileLength; i++) {       		
	    		String ipAdress = generateIpAdress();
	    		String writeLine = String.format("%15s - - %d\n", ipAdress, i);
                writer.write(writeLine);
	    	}	 
	    	writer.close();
            } catch (IOException e) {
            e.printStackTrace();
            }
    }
}
