package errors;

public class UnsupportedLogFileFormatException extends Exception {

	public UnsupportedLogFileFormatException() {
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLogFileFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLogFileFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLogFileFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public UnsupportedLogFileFormatException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
