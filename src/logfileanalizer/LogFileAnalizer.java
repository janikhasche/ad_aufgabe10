package logfileanalizer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import errors.UnsupportedLogFileFormatException;
import hashtable.HashTable;

public class LogFileAnalizer {
	
	public HashTable<List> logTable;
	private Set<String> ips;
	private Pattern logLinePattern;

	public LogFileAnalizer(String file, HashTable<List> ipTexts) throws IOException, UnsupportedLogFileFormatException {
		this.logTable = ipTexts;
		logLinePattern = Pattern.compile("(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})(.*)");
		ips = new TreeSet();
		parseFile(file);	
	}
	
	public Set<String> getIps(){
		return ips;
	}
	
	
	public List<String> getTextsfromIp(String ipString) throws UnsupportedLogFileFormatException{
		long ipLong = getIpFromLine(ipString);
		return logTable.get(ipLong);
	}
	
	
	private String convertIpLongToString(long ip) {
		long[] ipParts = new long[4];
		ipParts[0] = (long) (ip % (Math.pow(10, 3))); 
		ipParts[1] = (long) ((ip / Math.pow(10, 3)) % (Math.pow(10, 3))); 
		ipParts[2] = (long) ((ip / Math.pow(10, 6)) % (Math.pow(10, 3))); 
		ipParts[3] = (long) ((ip / Math.pow(10, 9)) % (Math.pow(10, 3))); 
		return String.format("%d.%d.%d.%d", ipParts[3], ipParts[2], ipParts[1], ipParts[0]);
	}
	
	
	private void parseFile(String file) throws IOException, UnsupportedLogFileFormatException{
		 BufferedReader br = new BufferedReader(new FileReader(file));
		    String line;
		    while ((line = br.readLine()) != null) {
		    	long lineIp = getIpFromLine(line);
		    	String lineText = getTextFromLine(line);
		    	insertToTable(lineIp, lineText);
		    	ips.add(convertIpLongToString(lineIp));
		    }
	}
	
	
	private void insertToTable(long ip, String text) {
		List ipList = logTable.get(ip);
		if(logTable.get(ip) != null){
			ipList.add(text);
		}else{
			List newList = new LinkedList();
			newList.add(text);
			logTable.insert(ip, newList);
		}
	}
	
	
	private long getIpFromLine(String logString) throws UnsupportedLogFileFormatException{
		long ip;
		String ipString = "";
		
		Matcher m = logLinePattern.matcher(logString);	
		if(m.find()){	
			// fill single parts of ip with 0, e.g 15 -> 015
			for(int i = 1; i <= 4; i++){
				String ipPart = m.group(i);
				for(int j = ipPart.length(); j < 3; j++){
					ipPart = "0" + ipPart;
				}
				ipString += ipPart;
			}
			ip = Long.parseLong(ipString);		
		}else{
			throw new UnsupportedLogFileFormatException("Unsupported Line: " + logString);
		}
		
		return ip;
	}
	
	
	private String getTextFromLine(String logString) throws UnsupportedLogFileFormatException{
		String text;
		
		Matcher m = logLinePattern.matcher(logString);	
		if(m.find()){
			text = m.group(5);
		}else{
			throw new UnsupportedLogFileFormatException("Unsupported Line: " + logString);
		}
		
		return text;
	}
}
