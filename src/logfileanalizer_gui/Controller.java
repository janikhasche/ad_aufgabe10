package logfileanalizer_gui;

import hashtable.DoubleHashTable;
import hashtable.HashTableStub;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import errors.UnsupportedLogFileFormatException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import logfileanalizer.LogFileAnalizer;


public class Controller {

    @FXML
    private ResourceBundle resources;
    private MainApp mainApp;
    
    private LogFileAnalizer logFileAnalizer;

    @FXML
    private URL location;

    @FXML
    private ListView<String> ipList;

    @FXML
    private Button openLogFileButton;

    @FXML
    private ListView<String> textList;


    @FXML
    void doFileChooserDialog(ActionEvent event) {
    	//TODO add dialog	
    	try {
			logFileAnalizer = new LogFileAnalizer("LogFile.txt", new DoubleHashTable());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnsupportedLogFileFormatException e) {
            e.printStackTrace();
        }
        setIpList();
    }

    @FXML
    void showIpAdressText(MouseEvent event) throws UnsupportedLogFileFormatException {
    	String selectedIpAdress = ipList.getSelectionModel().getSelectedItem();
    	if(selectedIpAdress != null){
    		setTextList(selectedIpAdress);
    	}
    }

    @FXML
    void initialize() {
        assert ipList != null : "fx:id=\"ipList\" was not injected: check your FXML file 'gui_layout.fxml'.";
        assert openLogFileButton != null : "fx:id=\"openLogFileButton\" was not injected: check your FXML file 'gui_layout.fxml'.";
        assert textList != null : "fx:id=\"textList\" was not injected: check your FXML file 'gui_layout.fxml'.";
    }

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;		
	}
	
	private void setIpList() {
		ObservableList<String> items = FXCollections.observableArrayList();
		items.addAll(logFileAnalizer.getIps());
		ipList.setItems(items);
	}
	
	private void setTextList(String ipString) throws UnsupportedLogFileFormatException {
		ObservableList<String> items = FXCollections.observableArrayList();
		items.addAll(logFileAnalizer.getTextsfromIp(ipString));
		textList.setItems(items);
	}

}
